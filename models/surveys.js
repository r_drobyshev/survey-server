'use strict';
module.exports = (sequelize, DataTypes) => {
  const Surveys = sequelize.define('Surveys', {
    survey_id: {
      type: DataTypes.STRING,
      primaryKey: true
    },
    user_id: DataTypes.STRING,
    name: DataTypes.STRING,
    description: DataTypes.STRING,
    date: DataTypes.DATE,
    numb_of_res: DataTypes.INTEGER,
    password: DataTypes.STRING,
    state: DataTypes.BOOLEAN,
  }, {});
  Surveys.associate = function(models) {
    Surveys.belongsTo(models.Users, { foreignKey: 'user_id' });
    Surveys.hasMany(models.Questions, { foreignKey: 'survey_id', as: 'questions' }, { onDelete: 'CASCADE' });
    Surveys.hasMany(models.Results, { foreignKey: 'survey_id', as: 'results' }, { onDelete: 'CASCADE' });
  };
  return Surveys;
};