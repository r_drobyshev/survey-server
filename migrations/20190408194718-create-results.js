'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Results', {
      answer_id: {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false,
      },
      respondent_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
      },
      survey_id: {
        type: Sequelize.STRING,
        onDelete: 'CASCADE',
        references: {
          model: 'Surveys',
          key: 'survey_id',
        },
        allowNull: false,
      },
      question_id: {
        type: Sequelize.STRING,
        references: {
          model: 'Questions',
          key: 'question_id',
        },
        allowNull: false,
      },
      block_id: {
        type: Sequelize.STRING
      },
      answer: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: new Date(),
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: new Date(),
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Results');
  }
};