'use strict';
module.exports = (sequelize, DataTypes) => {
  const Results = sequelize.define('Results', {
    answer_id: {
      type: DataTypes.STRING,
      primaryKey: true
    },
    respondent_id: {
      type: DataTypes.STRING,
    },
    block_id: DataTypes.STRING,
    answer: DataTypes.STRING
  }, {});
  Results.associate = function(models) {
    Results.belongsTo(models.Surveys, { foreignKey: 'survey_id' });
    Results.belongsTo(models.Questions, { foreignKey: 'question_id' });
  };
  return Results;
};