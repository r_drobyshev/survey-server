const randomize = require('randomatic');
const jsonfile = require('jsonfile');
const path = require('path');


const Surveys = require('../models').Surveys;
const Questions = require('../models').Questions;
const Users = require('../models').Users;
const Results = require('../models').Results;


// Results.findAll()
//   .then(result => {
//     const file = path.resolve(__dirname, '../tmp/data.json');
//     jsonfile.writeFile(file, result, function (err) {
//       if (err) console.error(err)
//     });
//     // console.log(JSON.stringify(result));
//   })
//   .catch(err => console.log('-->err', err));

Results.destroy({truncate: true});