'use strict';
const moment = require('moment');
moment.locale('ru');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Surveys', [{
      survey_id: '0',
      user_id: '1',
      name: 'Название опроса',
      description: 'Template for creating surveys',
      date: null,
      numb_of_res: null,
      password: null,
      state: false,
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Surveys', null, {});
  }
};
