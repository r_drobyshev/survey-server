const jwt = require('jsonwebtoken');
const Users = require('../models').Users;
const md5 = require('md5');

async function signIn(req, res) {
  try {
    const username = req.body.email;
    const password = req.body.password;
    const user = await Users.findOne({ where: { username }});
    if (!user) return res.status(401).json('Пользователь не найден!');
    if (md5(password) !== user.password) return res.status(401).json('Введите верный пароль!');
    const payload = {
      username: user.username,
    };
    const token = await jwt.sign(payload, '2clX1hP8CTHwxjeS', { expiresIn: 60 * 60 * 24, algorithm: 'HS256' });
    const tokenWithBearer = 'Bearer ' + token;
    res.status(200).json(tokenWithBearer);
  } catch (err) {
    res.status(500).send(err);
  }
}

module.exports = {
  signIn,
};