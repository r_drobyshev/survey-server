function s2ab(s) {
  const buf = new ArrayBuffer(s.length); //convert s to arrayBuffer
  const view = new Uint8Array(buf);  //create uint8array as viewer
  for (let i=0; i<s.length; i++) view[i] = s.charCodeAt(i) & 0xFF; //convert to octet
  return buf;
}

function getAnswer(blockId, answer, id) {
  console.log('-->answer, id', blockId, answer, id);
  let resultAnswer = 0;
  switch (blockId) {
    case '3':
      switch (id) {
        case 1:
        case 2:
        case 3:
        case 5:
        case 6:
          switch (JSON.parse(answer)) {
            case '1': resultAnswer = 5; break;
            case '2': resultAnswer = 4; break;
            case '3': resultAnswer = 2; break;
            case '4': resultAnswer = 1; break;
            case '5': resultAnswer = 3; break;
            default:
              break;
          }
          break;
        case 4:
          switch (JSON.parse(answer)) {
            case '1': resultAnswer = 1; break;
            case '2': resultAnswer = 2; break;
            case '3': resultAnswer = 4; break;
            case '4': resultAnswer = 5; break;
            case '5': resultAnswer = 3; break;
            default:
              break;
          }
          break;
        case 7:
          switch (JSON.parse(answer)) {
            case '1': resultAnswer = 5; break;
            case '2': resultAnswer = 4; break;
            case '3': resultAnswer = 2; break;
            case '4': resultAnswer = 1; break;
            case '5': resultAnswer = 3; break;
            default:
              break;
          }
          break;
      }
      break;
    case '7':
      switch (id) {
        case 1:
        case 2:
        case 4:
        case 5:
        case 6:
          switch (JSON.parse(answer)) {
            case '1': resultAnswer = 5; break;
            case '2': resultAnswer = 4; break;
            case '3': resultAnswer = 2; break;
            case '4': resultAnswer = 1; break;
            case '5': resultAnswer = 3; break;
            default:
              break;
          }
          break;
        case 3:
          switch (JSON.parse(answer)) {
            case '1': resultAnswer = 5; break;
            case '2': resultAnswer = 4; break;
            case '3': resultAnswer = 3; break;
            case '4': resultAnswer = 2; break;
            case '5': resultAnswer = 1; break;
            default:
              break;
          }
          break;
        case 7:
          switch (JSON.parse(answer)) {
            case '1': resultAnswer = 5; break;
            case '2': resultAnswer = 4; break;
            case '3': resultAnswer = 2; break;
            case '4': resultAnswer = 1; break;
            case '5': resultAnswer = 3; break;
            default:
              break;
          }
      }
      break;
  }
  return resultAnswer;
}

function compareBlocks(a, b) {
  if (+a <= +b) {
    return -1;
  }
  if (+a > +b) {
    return 1;
  }
  return 0;
}

const compareByBlockId = (a, b) => {
  if (+a.block_id < +b.block_id) {
    return -1;
  }
  if (+a.block_id >= +b.block_id) {
    return 1;
  }
  return 0;
};

function isJsonString(str) {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
}

module.exports = {
  s2ab,
  getAnswer,
  compareBlocks,
  compareByBlockId,
  isJsonString,
};