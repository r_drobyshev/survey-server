'use strict';
module.exports = (sequelize, DataTypes) => {
  const Questions = sequelize.define('Questions', {
    question_id: {
      type: DataTypes.STRING,
      primaryKey: true
    },
    id: DataTypes.INTEGER,
    block_id: DataTypes.STRING,
    text: DataTypes.TEXT,
    type: DataTypes.STRING,
    answer_variants: DataTypes.STRING
  }, {});
  Questions.associate = function(models) {
    Questions.hasMany(models.Results, { foreignKey: 'question_id', as: 'results'});
    Questions.belongsTo(models.Surveys, { foreignKey: 'survey_id', onDelete: 'CASCADE' });
  };
  return Questions;
};