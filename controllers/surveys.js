const randomize = require('randomatic');
const moment = require('moment');
moment.locale('ru');
const _ = require('lodash');

const Surveys = require('../models').Surveys;
const Questions = require('../models').Questions;
const Users = require('../models').Users;
const Results = require('../models').Results;
const sequelize = require('../models').sequelize;
const Sequelize = require('../models').Sequelize;

const { getAnswer, compareBlocks, compareByBlockId } = require('../utils');

function createSurvey(req, res) {
  const survey_id = randomize('Aa0', 8);
  const { user_id, name, description } = req.body.survey;
  const surveyParams = {
    survey_id,
    user_id,
    name,
    description,
    date: moment().format(),
    numb_of_res: 0,
    password: randomize('Aa0', 8),
    state: true,
  };
  const questions = req.body.questions.map((question) => {
    const { block_id, text, type, answer_variants, id } = question;
    return {
      question_id: randomize('Aa0', 8),
      id,
      survey_id,
      block_id,
      text,
      type,
      answer_variants,
    }
  });
  sequelize.transaction(async t => {
    try {
      await Surveys.create(surveyParams, {transaction: t});
      await Questions.bulkCreate(questions, {transaction: t});
    } catch (err) {
      res.status(500).send(err);
    }
  })
    .then(result => res.status(200).json(result))
    .catch((err) => res.status(500).send(err));
}

function saveSurvey(req, res) {
  const { user_id, name, description, survey_id, password, numb_of_res, date } = req.body.survey;
  const surveyParams = {
    survey_id,
    user_id,
    name,
    description,
    date,
    numb_of_res,
    password,
  };
  sequelize.transaction(async t => {
    try {
      async function multipleUpdate() {
        const items = [];
        req.body.questions.forEach(question => {
          const { question_id } = question;
          Questions.update(question, { where: { question_id }, fields: ["text", "answer_variants"] }).then(data => items.push(data));
        });
        return items;
      }
      await Surveys.update(surveyParams, {transaction: t, where: { survey_id }});
      await multipleUpdate();
    } catch (err) {
      res.status(500).send(err);
    }
  })
    .then(result => res.status(200).json(result))
    .catch((err) => res.status(500).send(err));
}

function getSurveys(req, res) {
  return Surveys.findAll({ order: [['createdAt', 'DESC']] })
    .then(result => res.status(200).json(result))
    .catch((err) => res.status(500).send(err));
}

async function adminGetSurveyById(req, res) {
  const survey_id = req.params.id;
  try {
    const questions = await Questions.findAll({
      where: { survey_id },
      attributes: ['answer_variants', 'block_id', 'question_id', 'survey_id', 'text', 'type', 'id'],
      order: [
        ['id', 'ASC'],
      ],
    });
    const survey = await Surveys.findByPk(survey_id);
    res.status(200).json({ survey, questions })
  } catch (err) {
    console.log('-->err', err);
    res.status(500).send(err)
  }
}

async function clientGetSurveyById(req, res) {
  const survey_id = req.params.id;
  try {
    const questions = await Questions.findAll({ where: { survey_id }, order: [['id', 'ASC']] });
    const survey = await Surveys.findByPk(survey_id, { attributes: ['name', 'description', 'survey_id', 'state'] });
    res.status(200).json({ survey, questions })
  } catch (err) {
    res.status(500).send(err)
  }
}

function createUser(req, res) {
  Users.create({
    user_id: randomize('Aa0', 8),
    username: 'r.drobyshev@dunice.net',
    password: '12345678',
  })
    .then(result => res.status(200).json(result))
    .catch(err => res.status(500).send(err));
}

async function checkPassword(req, res) {
  try {
    const surveyId = req.params.id;
    const survey = await Surveys.findByPk(surveyId);
    if (survey.password === req.body.password) {
      const token = `${randomize('Aa0', 16)}-${Date.now()}+${randomize('0', 16)}`;
      res.status(200).json(token);
    }
    else res.status(403).json('Неверный пароль!');
  } catch (err) {
    res.status(500).send(err);
  }
}

async function saveResults(req, res) {
  try {
    const survey = req.body.survey;
    const currentSurvey = await Surveys.findByPk(survey.survey_id);
    const respondent_id = currentSurvey.numb_of_res + 1;
    // const question_ids = req.body.results.map(result => result.question_id);
    const question_ids = await Questions.findAll({
      where: { survey_id: req.params.id },
      order: [
        ['block_id', 'ASC'],
        ['id', 'ASC'],
      ],
    })
      .map(q => q.dataValues)
      .map(q => q.question_id);
    const sortedResults = question_ids.map(questionId => req.body.results.find(result => result.question_id === questionId));
    const questions = await Questions.findAll().map(q => q.dataValues);
    const results = sortedResults.map(result => {
      const block_id = questions.find(q => q.question_id === result.question_id).block_id;
      if (block_id === '3') return {
        survey_id: result.survey_id,
        question_id: result.question_id,
        respondent_id,
        answer: getAnswer(block_id, result.answer, questions.find(q => q.question_id === result.question_id).id),
        answer_id: randomize('Aa0', 16),
        block_id: questions.find(q => q.question_id === result.question_id).block_id,
      };
      if (block_id === '4' || block_id === '5.1' || block_id === '5.2' || block_id === '8.1' || block_id === '8.2' || block_id === '9' || block_id === '10' || block_id === '1') return {
        survey_id: result.survey_id,
        question_id: result.question_id,
        respondent_id,
        answer: JSON.parse(result.answer),
        answer_id: randomize('Aa0', 16),
        block_id: questions.find(q => q.question_id === result.question_id).block_id,
      };
      if (block_id === '6') return {
        survey_id: result.survey_id,
        question_id: result.question_id,
        respondent_id,
        answer: JSON.stringify(JSON.parse(result.answer).map((answer, index) => answer ? index : 'empty').filter(answer => typeof answer !== 'string')),
        answer_id: randomize('Aa0', 16),
        block_id: questions.find(q => q.question_id === result.question_id).block_id,
      };
      if (block_id === '7') return {
        survey_id: result.survey_id,
        question_id: result.question_id,
        respondent_id,
        answer: getAnswer(block_id, result.answer, questions.find(q => q.question_id === result.question_id).id),
        answer_id: randomize('Aa0', 16),
        block_id: questions.find(q => q.question_id === result.question_id).block_id,
      };
      return {
        survey_id: result.survey_id,
        question_id: result.question_id,
        respondent_id,
        answer: result.answer,
        answer_id: randomize('Aa0', 16),
        block_id: questions.find(q => q.question_id === result.question_id).block_id,
      }
    });
    await Results.bulkCreate(results);
    const updatedSurvey = { ...currentSurvey, numb_of_res: currentSurvey.numb_of_res + 1 };
    await Surveys.update(updatedSurvey, { where: { survey_id: survey.survey_id }, fields: ["numb_of_res",] });
    res.status(200).json('Saved!');
  } catch (err) {
    console.log('-->err', err);
    res.status(500).send(err.stack);
  }
}

function isJsonString(str) {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
}

async function getResultsBySurveyId(req, res) {
  try {
    if (req.query.blockId) {
      console.log('-->req.query.blockId', req.query.blockId);
      const blockId = req.query.blockId;
      const data = await Surveys.findOne({
        where: { survey_id: req.params.id },
      });
      const allQuestions = await Questions.findAll({
        where: { survey_id: req.params.id },
        order: [['block_id', 'ASC'], ['id', 'ASC']],
      }).map(q => q.dataValues);
      if (!data) {
        const data = await Surveys.findOne({
          where: { survey_id: req.params.id },
          include: [
            {
              model: Questions,
              as: 'questions',
              order: [['id', 'ASC']],
            },
          ]
        });
        const { survey_id, name, numb_of_res, questions } = data;
        const blocks = questions.map(q => q.dataValues).map(item => item.block_id).sort(compareBlocks).filter(((item, index, block_ids) => block_ids.indexOf(item) === index));
        return res.status(200).json({ survey: { survey_id, name, numb_of_res, blocks }, results: [] });
      }
      const { survey_id, name, numb_of_res, results, questions } = data;

      const blocks = allQuestions.map(item => item.block_id).sort(compareBlocks).filter(((item, index, block_ids) => block_ids.indexOf(item) === index));
      if (blockId === '1') {
        const questions = await Questions.findAll({
          where: {block_id: blockId, survey_id: req.params.id},
          order: [['block_id', 'ASC'], ['id', 'ASC']],
        }).map(q => q.dataValues);
        const dataResults = await Results.findAll({
          where: {block_id: blockId, survey_id: req.params.id},
          order: [['respondent_id', 'ASC']],
        }).map(r => r.dataValues);
        const gruppedByRespondentData = _.groupBy(dataResults, 'respondent_id');
        const results = Object.values(gruppedByRespondentData).map((resultsByResp, index) => {
          const resultsByQuestion = questions.map((q) => {
            const { question_id } = q;
            return resultsByResp.find(res => res.question_id === question_id).answer;
          });
          // const results = resultsByResp.map(res => res.answer);
          const answers = questions.map(q => {
            const { answer_variants } = q;
            if (isJsonString(q.answer_variants)) return JSON.parse(answer_variants);
            return answer_variants
          });
          // console.log('-->answers', answers);
          return {
            respondent: {
              question: "№ Респондента",
              number: resultsByResp[0].respondent_id,
            },
            question_1: {
              question: questions.find(q => q.id === 1).text,
              answer: answers[0][parseInt(resultsByQuestion[0]) - 1],
            },
            question_2: {
              question: questions.find(q => q.id === 2).text,
              answer: answers[1][parseInt(resultsByQuestion[1]) - 1],
            },
            question_3: {
              question: questions.find(q => q.id === 3).text,
              answer: answers[2][parseInt(resultsByQuestion[2]) - 1],
            },
            question_4: {
              question: questions.find(q => q.id === 4).text,
              answer: resultsByQuestion[3],
            },
            question_5: {
              question: questions.find(q => q.id === 5).text,
              answer: answers[4][parseInt(resultsByQuestion[4]) - 1],
            },
            question_6: {
              question: questions.find(q => q.id === 6).text,
              answer: resultsByQuestion[5],
            },
            question_7: {
              question: questions.find(q => q.id === 7).text,
              answer: answers[6][parseInt(resultsByQuestion[6]) - 1],
            },
            question_8: {
              question: questions.find(q => q.id === 8).text,
              answer: answers[7][parseInt(resultsByQuestion[7]) - 1],
            },
            question_9: {
              question: questions.find(q => q.id === 9).text,
              answer: answers[8][parseInt(resultsByQuestion[8]) - 1],
            },
          };
        });
        return res.status(200).json({ survey: { survey_id, name, numb_of_res, blocks }, results });
      }
      if (blockId === '2') {
        const data = await Surveys.findOne({
          where: { survey_id: req.params.id },
          include: [
            {
              model: Results,
              as: 'results',
              where: { block_id: blockId },
            },
            {
              model: Questions,
              as: 'questions',
              where: { block_id: blockId },
              order: [['id', 'ASC']],
            },
          ]
        });
        const results = data.questions.map(question => {
          const { question_id, answer_variants, text, type, block_id } = question;
          const count = data.results.reduce((acc, result) => {
            if (result.question_id === question_id) {
              if (type !== 'number') {
                const answers = JSON.parse(result.answer).map((answer, index) => answer === true && index).filter(answer => answer);
                return acc.concat(answers)
              }
              if (type !== 'multi' && type !== 'single') {
                return acc.concat(result.answer)
              }
            }
            return acc;
          }, []);
          const totalCount = {};
          count.forEach((findedValue, index, array) => {
            totalCount[findedValue] = (array.reduce((acc, value) => value === findedValue ? acc + 1: acc, 0) / data.numb_of_res) * 100
          });
          return { question_id, text, answer_variants, totalCount, type, block_id };
        });
        return res.status(200).json({ survey: { survey_id, name, numb_of_res, blocks }, results });
      }
      if (blockId === '3') {
        const data = await Results.findAll({
          where: { block_id: blockId, survey_id: req.params.id },
          order: [['respondent_id', 'ASC']],
        }).map(r => r.dataValues);
        const gruppedByRespondentData = _.groupBy(data, 'respondent_id');
        const results = Object.values(gruppedByRespondentData).map(respondent => {
          return {
            respondent: respondent[0].respondent_id,
            block_id: respondent[0].block_id,
            total: Math.round(respondent.reduce((acc, result) => (acc + parseInt(result.answer)), 0) / 6 * 100) / 100,
          }
        });
        results.push({ respondent: 'Общий интегральный показатель', block_id: '3', total: results.reduce((acc, res) => acc + res.total, 0)/numb_of_res });
        return res.status(200).json({ survey: { survey_id, name, numb_of_res, blocks }, results });
      }
      if (blockId === '4') {
        const questions = await Questions.findAll({
          where: { block_id: blockId, survey_id: req.params.id },
          order: [['block_id', 'ASC'], ['id', 'ASC']],
        }).map(q => q.dataValues);
        const groupAQuestions = questions.filter(question => question.id === 1 || question.id === 2 || question.id === 3 || question.id === 4 || question.id === 5 || question.id === 11)
          .map(question => question.question_id);
        const groupBQuestions = questions.filter(question => question.id === 9 || question.id === 12 || question.id === 13)
          .map(question => question.question_id);
        const groupCQuestions = questions.filter(question => question.id === 6 || question.id === 7 || question.id === 8 || question.id === 10 || question.id === 16)
          .map(question => question.question_id);
        const groupDQuestions = questions.filter(question => question.id === 14 || question.id === 15 || question.id === 17 || question.id === 18 || question.id === 19)
          .map(question => question.question_id);
        const data = await Results.findAll({
          where: { block_id: blockId, survey_id: req.params.id },
          order: [['respondent_id', 'ASC']],
        }).map(r => r.dataValues);
        const gruppedByRespondentData = _.groupBy(data, 'respondent_id');
        const results = Object.values(gruppedByRespondentData).map(resultsByResp => {
          const groupATotal = groupAQuestions.map(questionId => resultsByResp.find(result => result.question_id === questionId))
            .reduce((acc, result) => acc + Number(result.answer), 0) / groupAQuestions.length;
          const groupBTotal = groupBQuestions.map(questionId => resultsByResp.find(result => result.question_id === questionId))
            .reduce((acc, result) => acc + Number(result.answer), 0)/groupBQuestions.length;
          const groupCTotal = groupCQuestions.map(questionId => resultsByResp.find(result => result.question_id === questionId))
            .reduce((acc, result) => acc + Number(result.answer), 0)/groupCQuestions.length;
          const groupDTotal = groupDQuestions.map(questionId => resultsByResp.find(result => result.question_id === questionId))
            .reduce((acc, result) => acc + Number(result.answer), 0)/groupDQuestions.length;
          return {
            respondent: resultsByResp[0].respondent_id,
            block_id: resultsByResp[0].block_id,
            totalA: Math.round(groupATotal * 100) / 100,
            totalB: Math.round(groupBTotal * 100) / 100,
            totalC: Math.round(groupCTotal * 100) / 100,
            totalD: Math.round(groupDTotal * 100) / 100,
          };
        });
        const totalResults = [];
        totalResults.push({
          title: 'Средний интегральный показатель (Престижно-материальные мотивы)',
          block_id: '4',
          total: results.reduce((acc, res) => acc + res.totalA, 0)/numb_of_res,
        });
        totalResults.push({
          title: 'Средний интегральный показатель (Мотивы самореализации)',
          block_id: '4',
          total: results.reduce((acc, res) => acc + res.totalB, 0)/numb_of_res,
        });
        totalResults.push({
          title: 'Средний интегральный показатель (Мотивы построения карьеры)',
          block_id: '4',
          total: results.reduce((acc, res) => acc + res.totalC, 0)/numb_of_res,
        });
        totalResults.push({
          title: 'Средний интегральный показатель (Формальные мотивы и мотивы «легкой жизни»)',
          block_id: '4',
          total: results.reduce((acc, res) => acc + res.totalD, 0)/numb_of_res,
        });
        return res.status(200).json({ survey: { survey_id, name, numb_of_res, blocks }, results, totalResults });
      }
      if (blockId === '5.1' || blockId === '5.2') {
        const questions = await Questions.findAll({
          where: { block_id: blockId, survey_id: req.params.id },
          order: [['id', 'ASC']],
          // include: ['results'],
        }).map(q => q.dataValues);
        const data = await Results.findAll({
          where: { block_id: blockId, survey_id: req.params.id },
          order: [['respondent_id', 'ASC']],
        }).map(r => r.dataValues);

        const orderedResultsByQuestions = [];
        questions.forEach(q => {
          const arr = data.filter(res => res.question_id === q.question_id);
          orderedResultsByQuestions.push(...arr);
        });

        const gruppedByQuestionData = _.groupBy(orderedResultsByQuestions, 'question_id');
        const results = Object.values(gruppedByQuestionData).map(resultsByQuestion => {
          // console.log('-->resultsByQuestion', resultsByQuestion);
          const group_1 = resultsByQuestion.reduce((acc, result) => {
            if (result.answer === '1') return acc + 1;
            return acc;
          }, 0);
          const group_2 = resultsByQuestion.reduce((acc, result) => {
            if (result.answer === '2') return acc + 1;
            return acc;
          }, 0);
          const group_3 = resultsByQuestion.reduce((acc, result) => {
            if (result.answer === '3') return acc + 1;
            return acc;
          }, 0);
          const group_4 = resultsByQuestion.reduce((acc, result) => {
            if (result.answer === '4') return acc + 1;
            return acc;
          }, 0);
          const group_5 = resultsByQuestion.reduce((acc, result) => {
            if (result.answer === '5') return acc + 1;
            return acc;
          }, 0);
          const total = resultsByQuestion.reduce((acc, result) => acc + Number(result.answer), 0);
          return {
            average: Math.round(total/questions.length * 100)/100,
            group_1: Math.round(group_1/numb_of_res * 100)/100 * 100,
            group_2: Math.round(group_2/numb_of_res * 100)/100 * 100,
            group_3: Math.round(group_3/numb_of_res * 100)/100 * 100,
            group_4: Math.round(group_4/numb_of_res * 100)/100 * 100,
            group_5: Math.round(group_5/numb_of_res * 100)/100 * 100,
            block_id: resultsByQuestion[0].block_id,
          };
        });
        return res.status(200).json({ survey: { survey_id, name, numb_of_res, blocks }, results });
      }
      if (blockId === '6') {
        const questions = await Questions.findAll({
          where: { block_id: blockId, survey_id: req.params.id },
          order: [['block_id', 'ASC'], ['id', 'ASC']],
        }).map(q => q.dataValues);
        const data = await Results.findAll({
          where: { block_id: blockId, survey_id: req.params.id },
          order: [['respondent_id', 'ASC']],
        }).map(r => r.dataValues);

        const orderedResultsByQuestions = [];
        questions.forEach(q => {
          const arr = data.filter(res => res.question_id === q.question_id);
          orderedResultsByQuestions.push(...arr);
        });

        const gruppedByQuestion = _.groupBy(orderedResultsByQuestions, 'question_id');
        const results = Object.values(gruppedByQuestion).map(resultsByQuestion => {
          const count = resultsByQuestion.reduce((acc, result) => {
            const answers = JSON.parse(result.answer);
            return acc.concat(answers);
          }, []);
          const totalCount = {};
          count.forEach((findedValue, index, array) => {
            totalCount[findedValue] = (array.reduce((acc, value) => value === findedValue ? acc + 1: acc, 0) / numb_of_res) * 100;
          });
          return { totalCount, question_id: resultsByQuestion[0].question_id };
        });
        const superResults = results.map(result => {
          const questionInfo = questions.find((q) => q.question_id === result.question_id);
          const { totalCount } = result;
          const { question_id, block_id, text, type, answer_variants } = questionInfo;
          return {
            question_id,
            block_id,
            text,
            type,
            totalCount,
            answer_variants,
          }
        });
        return res.status(200).json({ survey: { survey_id, name, numb_of_res, blocks }, results: superResults });
      }
      if (blockId === '7') {
        const data = await Results.findAll({
          where: { block_id: blockId, survey_id: req.params.id },
          order: [['respondent_id', 'ASC']],
        }).map(r => r.dataValues);
        const gruppedByRespondentData = _.groupBy(data, 'respondent_id');
        const integralResults = Object.values(gruppedByRespondentData).map(respondent => {
          return {
            respondent: respondent[0].respondent_id,
            block_id: respondent[0].block_id,
            total: Math.round(respondent.reduce((acc, result) => (acc + parseInt(result.answer)), 0) / 6 * 100) / 100,
          }
        });
        integralResults.push({ respondent: 'Общий интегральный показатель', block_id: '7', total: integralResults.reduce((acc, res) => acc + res.total, 0)/numb_of_res });

        const questions = await Questions.findAll({
          where: { block_id: blockId, survey_id: req.params.id },
          order: [['block_id', 'ASC'], ['id', 'ASC']],
        }).map(q => q.dataValues);
        const orderedResultsByQuestions = [];
        questions.forEach(q => {
          const arr = data.filter(res => res.question_id === q.question_id);
          orderedResultsByQuestions.push(...arr);
        });
        const gruppedByQuestionData = _.groupBy(orderedResultsByQuestions, 'question_id');
        const raspredResults = Object.values(gruppedByQuestionData).map(resultsByQuestion => {
          const group_1 = resultsByQuestion.reduce((acc, result) => {
            if (result.answer === '1') return acc + 1;
            return acc;
          }, 0);
          const group_2 = resultsByQuestion.reduce((acc, result) => {
            if (result.answer === '2') return acc + 1;
            return acc;
          }, 0);
          const group_3 = resultsByQuestion.reduce((acc, result) => {
            if (result.answer === '3') return acc + 1;
            return acc;
          }, 0);
          const group_4 = resultsByQuestion.reduce((acc, result) => {
            if (result.answer === '4') return acc + 1;
            return acc;
          }, 0);
          const group_5 = resultsByQuestion.reduce((acc, result) => {
            if (result.answer === '5') return acc + 1;
            return acc;
          }, 0);
          const total = resultsByQuestion.reduce((acc, result) => acc + Number(result.answer), 0);
          return {
            average: Math.round(total/questions.length * 100)/100,
            group_1: Math.round(group_1/numb_of_res * 100)/100 * 100,
            group_2: Math.round(group_2/numb_of_res * 100)/100 * 100,
            group_3: Math.round(group_3/numb_of_res * 100)/100 * 100,
            group_4: Math.round(group_4/numb_of_res * 100)/100 * 100,
            group_5: Math.round(group_5/numb_of_res * 100)/100 * 100,
            block_id: resultsByQuestion[0].block_id,
          };
        });
        return res.status(200).json({ survey: { survey_id, name, numb_of_res, blocks }, results: { integralResults, raspredResults } })
      }
      if (blockId === '8.1') {
        const questions = await Questions.findAll({
          where: { block_id: blockId, survey_id: req.params.id },
          order: [['block_id', 'ASC'], ['id', 'ASC']],
        }).map(q => q.dataValues);
        const groupAQuestions = questions.filter(question => question.id === 2 || question.id === 3 || question.id === 4 || question.id === 5 || question.id === 16 || question.id === 17)
          .map(question => question.question_id);
        const groupBQuestions = questions.filter(question => question.id === 6 || question.id === 7 || question.id === 8 || question.id === 9|| question.id === 10 || question.id === 11)
          .map(question => question.question_id);
        const groupCQuestions = questions.filter(question => question.id === 12 || question.id === 13 || question.id === 14 || question.id === 15)
          .map(question => question.question_id);
        const groupDQuestions = questions.filter(question => question.id === 18 || question.id === 19 || question.id === 20 || question.id === 21)
          .map(question => question.question_id);
        const data = await Results.findAll({
          where: { block_id: blockId, survey_id: req.params.id },
          order: [['respondent_id', 'ASC']],
        }).map(r => r.dataValues);
        const gruppedByRespondentData = _.groupBy(data, 'respondent_id');
        const results = Object.values(gruppedByRespondentData).map(resultsByResp => {
          const groupATotal = groupAQuestions.map(questionId => resultsByResp.find(result => result.question_id === questionId))
            .reduce((acc, result) => acc + Number(result.answer), 0) / groupAQuestions.length;
          const groupBTotal = groupBQuestions.map(questionId => resultsByResp.find(result => result.question_id === questionId))
            .reduce((acc, result) => acc + Number(result.answer), 0)/groupBQuestions.length;
          const groupCTotal = groupCQuestions.map(questionId => resultsByResp.find(result => result.question_id === questionId))
            .reduce((acc, result) => acc + Number(result.answer), 0)/groupCQuestions.length;
          const groupDTotal = groupDQuestions.map(questionId => resultsByResp.find(result => result.question_id === questionId))
            .reduce((acc, result) => acc + Number(result.answer), 0)/groupDQuestions.length;
          return {
            respondent: resultsByResp[0].respondent_id,
            block_id: resultsByResp[0].block_id,
            totalA: Math.round(groupATotal * 100) / 100,
            totalB: Math.round(groupBTotal * 100) / 100,
            totalC: Math.round(groupCTotal * 100) / 100,
            totalD: Math.round(groupDTotal * 100) / 100,
          };
        });
        const totalResults = [];
        totalResults.push({
          title: 'Средний интегральный показатель значимости (Организационных условий)',
          block_id: '8.1',
          total: results.reduce((acc, res) => acc + res.totalA, 0)/numb_of_res,
        });
        totalResults.push({
          title: 'Средний интегральный показатель значимости (Материально-технической базы)',
          block_id: '8.1',
          total: results.reduce((acc, res) => acc + res.totalB, 0)/numb_of_res,
        });
        totalResults.push({
          title: 'Средний интегральный показатель значимости (Учебно-методического обеспечения)',
          block_id: '8.1',
          total: results.reduce((acc, res) => acc + res.totalC, 0)/numb_of_res,
        });
        totalResults.push({
          title: 'Средний интегральный показатель значимости (Качеств преподавателей)',
          block_id: '8.1',
          total: results.reduce((acc, res) => acc + res.totalD, 0)/numb_of_res,
        });
        return res.status(200).json({ survey: { survey_id, name, numb_of_res, blocks }, results, totalResults });
      }
      if (blockId === '8.2') {
        const questions = await Questions.findAll({
          where: { block_id: blockId, survey_id: req.params.id },
          order: [['block_id', 'ASC'], ['id', 'ASC']],
        }).map(q => q.dataValues);
        const groupAQuestions = questions.filter(question => question.id === 2 || question.id === 3 || question.id === 4 || question.id === 5 || question.id === 16 || question.id === 17)
          .map(question => question.question_id);
        const groupBQuestions = questions.filter(question => question.id === 6 || question.id === 7 || question.id === 8 || question.id === 9|| question.id === 10 || question.id === 11)
          .map(question => question.question_id);
        const groupCQuestions = questions.filter(question => question.id === 12 || question.id === 13 || question.id === 14 || question.id === 15)
          .map(question => question.question_id);
        const groupDQuestions = questions.filter(question => question.id === 18 || question.id === 19 || question.id === 20 || question.id === 21)
          .map(question => question.question_id);
        const data = await Results.findAll({
          where: { block_id: blockId, survey_id: req.params.id },
          order: [['respondent_id', 'ASC']],
        }).map(r => r.dataValues);
        const gruppedByRespondentData = _.groupBy(data, 'respondent_id');
        const results = Object.values(gruppedByRespondentData).map(resultsByResp => {
          const groupATotal = groupAQuestions.map(questionId => resultsByResp.find(result => result.question_id === questionId))
            .reduce((acc, result) => acc + Number(result.answer), 0) / groupAQuestions.length;
          const groupBTotal = groupBQuestions.map(questionId => resultsByResp.find(result => result.question_id === questionId))
            .reduce((acc, result) => acc + Number(result.answer), 0)/groupBQuestions.length;
          const groupCTotal = groupCQuestions.map(questionId => resultsByResp.find(result => result.question_id === questionId))
            .reduce((acc, result) => acc + Number(result.answer), 0)/groupCQuestions.length;
          const groupDTotal = groupDQuestions.map(questionId => resultsByResp.find(result => result.question_id === questionId))
            .reduce((acc, result) => acc + Number(result.answer), 0)/groupDQuestions.length;
          return {
            respondent: resultsByResp[0].respondent_id,
            block_id: resultsByResp[0].block_id,
            totalA: Math.round(groupATotal * 100) / 100,
            totalB: Math.round(groupBTotal * 100) / 100,
            totalC: Math.round(groupCTotal * 100) / 100,
            totalD: Math.round(groupDTotal * 100) / 100,
          };
        });
        const totalResults = [];
        totalResults.push({
          title: 'Средний интегральный показатель удовлетворенности (Организационными условиями)',
          block_id: '8.2',
          total: results.reduce((acc, res) => acc + res.totalA, 0)/numb_of_res,
        });
        totalResults.push({
          title: 'Средний интегральный показатель удовлетворенности (Материально-технической базой)',
          block_id: '8.2',
          total: results.reduce((acc, res) => acc + res.totalB, 0)/numb_of_res,
        });
        totalResults.push({
          title: 'Средний интегральный показатель удовлетворенности (Учебно-методическим обеспечением)',
          block_id: '8.2',
          total: results.reduce((acc, res) => acc + res.totalC, 0)/numb_of_res,
        });
        totalResults.push({
          title: 'Средний интегральный показатель удовлетворенности (Качествами преподавателей)',
          block_id: '8.2',
          total: results.reduce((acc, res) => acc + res.totalD, 0)/numb_of_res,
        });
        return res.status(200).json({ survey: { survey_id, name, numb_of_res, blocks }, results, totalResults });
      }
      if (blockId === '9') {
        const questions = await Questions.findAll({
          where: { block_id: blockId, survey_id: req.params.id },
          order: [['block_id', 'ASC'], ['id', 'ASC']],
        }).map(q => q.dataValues);
        const groupAQuestions = questions.filter(question => question.id === 1 || question.id === 11 || question.id === 21)
          .map(question => question.question_id);
        const groupBQuestions = questions.filter(question => question.id === 5 || question.id === 10 || question.id === 14 || question.id === 16 || question.id === 17 || question.id === 22 || question.id === 25)
          .map(question => question.question_id);
        const groupCQuestions = questions.filter(question => question.id === 2 || question.id === 4 || question.id === 7 || question.id === 8 || question.id === 12 || question.id === 15 || question.id === 19)
          .map(question => question.question_id);
        const groupDQuestions = questions.filter(question => question.id === 3 || question.id === 6 || question.id === 9 || question.id === 13 || question.id === 18 || question.id === 20 || question.id === 23)
          .map(question => question.question_id);
        const groupEQuestions = questions.filter(question => question.id === 24 || question.id === 26 || question.id === 27)
          .map(question => question.question_id);
        const data = await Results.findAll({
          where: { block_id: blockId, survey_id: req.params.id },
          order: [['respondent_id', 'ASC']],
        }).map(r => r.dataValues);
        const gruppedByRespondentData = _.groupBy(data, 'respondent_id');
        const results = Object.values(gruppedByRespondentData).map(resultsByResp => {
          const groupATotal = groupAQuestions.map(questionId => resultsByResp.find(result => result.question_id === questionId))
            .reduce((acc, result) => acc + Number(result.answer), 0) / groupAQuestions.length;
          const groupBTotal = groupBQuestions.map(questionId => resultsByResp.find(result => result.question_id === questionId))
            .reduce((acc, result) => acc + Number(result.answer), 0)/groupBQuestions.length;
          const groupCTotal = groupCQuestions.map(questionId => resultsByResp.find(result => result.question_id === questionId))
            .reduce((acc, result) => acc + Number(result.answer), 0)/groupCQuestions.length;
          const groupDTotal = groupDQuestions.map(questionId => resultsByResp.find(result => result.question_id === questionId))
            .reduce((acc, result) => acc + Number(result.answer), 0)/groupDQuestions.length;
          const groupETotal = groupEQuestions.map(questionId => resultsByResp.find(result => result.question_id === questionId))
            .reduce((acc, result) => acc + Number(result.answer), 0)/groupEQuestions.length;
          return {
            respondent: resultsByResp[0].respondent_id,
            block_id: resultsByResp[0].block_id,
            totalA: Math.round(groupATotal * 100) / 100,
            totalB: Math.round(groupBTotal * 100) / 100,
            totalC: Math.round(groupCTotal * 100) / 100,
            totalD: Math.round(groupDTotal * 100) / 100,
            totalE: Math.round(groupETotal * 100) / 100,
          };
        });
        const totalResults = [];
        totalResults.push({
          title: 'Оценки по видам трудностей (Мотивационные)',
          block_id: '9',
          total: results.reduce((acc, res) => acc + res.totalA, 0)/numb_of_res,
        });
        totalResults.push({
          title: 'Оценки по видам трудностей (Регулятивные)',
          block_id: '9',
          total: results.reduce((acc, res) => acc + res.totalB, 0)/numb_of_res,
        });
        totalResults.push({
          title: 'Оценки по видам трудностей (Операциональные)',
          block_id: '9',
          total: results.reduce((acc, res) => acc + res.totalC, 0)/numb_of_res,
        });
        totalResults.push({
          title: 'Оценки по видам трудностей (Социально-психологические)',
          block_id: '9',
          total: results.reduce((acc, res) => acc + res.totalD, 0)/numb_of_res,
        });
        totalResults.push({
          title: 'Оценки по видам трудностей (Бытовые)',
          block_id: '9',
          total: results.reduce((acc, res) => acc + res.totalE, 0)/numb_of_res,
        });
        return res.status(200).json({ survey: { survey_id, name, numb_of_res, blocks }, results, totalResults });
      }
      if (blockId === '10') {
        const questions = await Questions.findAll({
          where: {block_id: blockId, survey_id: req.params.id},
          order: [['block_id', 'ASC'], ['id', 'ASC']],
        }).map(q => q.dataValues);
        const dataResults = await Results.findAll({
          where: {block_id: blockId, survey_id: req.params.id},
          order: [['respondent_id', 'ASC']],
        }).map(r => r.dataValues);
        const filteredQuestionIds = questions.filter(question => question.id !== 15).map(question => question.question_id);
        const ignoredQuestionId = questions.find(question => question.id === 15).question_id;
        const gruppedByRespondentData = _.groupBy(dataResults, 'respondent_id');

        const orderedResultsByQuestions = [];
        questions.forEach(q => {
          const arr = dataResults.filter(res => res.question_id === q.question_id);
          orderedResultsByQuestions.push(...arr);
        });

        const gruppedByQuestionData = _.groupBy(orderedResultsByQuestions, 'question_id');
        delete gruppedByQuestionData[ignoredQuestionId];
        const aResults = Object.values(gruppedByRespondentData).map(resultsByResp => {
          const total = filteredQuestionIds.map(questionId => resultsByResp.find(result => result.question_id === questionId))
            .reduce((acc, result) => {
              if (Number(result.answer) !== 1) return acc;
              else return acc + Number(result.answer);
              }, 0) / filteredQuestionIds.length;
          return {
            respondent: resultsByResp[0].respondent_id,
            block_id: resultsByResp[0].block_id,
            total: Math.round(total * 100) / 100,
          };
        });

        const raspredResults = Object.values(gruppedByQuestionData).map(resultsByQuestion => {
          const group_1 = resultsByQuestion.reduce((acc, result) => {
            if (result.answer === '1') return acc + 1;
            return acc;
          }, 0);
          const group_2 = resultsByQuestion.reduce((acc, result) => {
            if (result.answer === '2') return acc + 1;
            return acc;
          }, 0);
          const group_3 = resultsByQuestion.reduce((acc, result) => {
            if (result.answer === '3') return acc + 1;
            return acc;
          }, 0);
          const total = resultsByQuestion.reduce((acc, result) => acc + Number(result.answer), 0);
          return {
            average: Math.round(total/filteredQuestionIds.length * 100)/100,
            group_1: Math.round(group_1/numb_of_res * 100)/100 * 100,
            group_2: Math.round(group_2/numb_of_res * 100)/100 * 100,
            group_3: Math.round(group_3/numb_of_res * 100)/100 * 100,
            block_id: resultsByQuestion[0].block_id,
          };
        });
        return res.status(200).json({ survey: { survey_id, name, numb_of_res, blocks }, results: { aResults, raspredResults } });
      }
      res.status(200).json({ survey: { survey_id, name, numb_of_res, blocks }, results: [] });
    } else {
      console.log('here_no-blockID');
      const data = await Surveys.findOne({
        where: { survey_id: req.params.id },
        include: [
          {
            model: Questions,
            as: 'questions',
          },
          // {
          //   model: Results,
          //   as: 'results',
          //   // where: { block_id: blockId },
          // },
        ]
      });
      // console.log('-->data.questions', data.questions.map(item => item.dataValues));
      const { questions, survey_id, name, numb_of_res, results } = data;
      const blocks = questions.map(item => item.dataValues).map(item => item.block_id).sort(compareBlocks).filter(((item, index, block_ids) => block_ids.indexOf(item) === index));
      res.status(200).json({ survey: { survey_id, name, numb_of_res, blocks }, results });
    }
    // console.log(results[0].results);
  } catch (err) {
    res.status(500).send(err.stack);
  }
}

async function deleteSurvey(req, res) {
  console.log('-->req.body', req.body);
  try {
    const deleted = await Surveys.destroy({
      where: {
        survey_id: req.body.surveyId,
      },
    });
    res.status(200).json(deleted)
  } catch (err) {
    res.status(500).send(err.stack);
  }
}

async function getResults(req, res) {
  try {
  } catch (e) {
  }
}

async function exportResults(req, res) {
  try {
    const survey_id = req.query.surveyId;
    // const data = await Surveys.findOne({
    //   where: { survey_id },
    // });
    const allQuestions = await Questions.findAll({
      where: { survey_id },
      order: [['block_id', 'ASC'], ['id', 'ASC']],
    }).map(q => q.dataValues);

    const blocks = allQuestions.map(item => item.block_id).sort(compareBlocks).filter(((item, index, block_ids) => block_ids.indexOf(item) === index));

    console.log('-->blocks', blocks);

    const dataResultsPromises = blocks.map((block_id) => new Promise((resolve, reject) => {
      return Results.findAll({
        where: { block_id, survey_id },
        order: [['respondent_id', 'ASC']],
      }).then((rawBlockData) => {
        const blockData = rawBlockData.map(r => r.dataValues);
        resolve(blockData);
      }).catch((err) => reject(err));
    }));

    const allResults = await Promise.all(dataResultsPromises);

    const flattenedResults = _.flatten(allResults);

    const question_ids = allQuestions.map(question => ({ question_id: question.question_id, text: question.text, block_id: question.block_id }));
    // console.log('-->question_ids', question_ids);
    // // const sortedQuestionsByBlockId = question_ids.sort(compareByBlockId);
    const sortedQuestionsByBlockId = _.orderBy(question_ids, [{ 'block_id': Number}], ['asc']);
    // console.log('-->sortedQuestionsByBlockId', sortedQuestionsByBlockId);
    // const dataResults = data.results.map(r => r.dataValues);
    const groupResultsByRespondentId = _.groupBy(flattenedResults, 'respondent_id');
    const results = Object.values(groupResultsByRespondentId).map((resultsByResp) => {
      return resultsByResp.map(result => {
        return {
          ['№ Респондента']: result.respondent_id,
          [sortedQuestionsByBlockId.find(question => question.question_id === result.question_id).text]: isJsonString(result.answer) && typeof JSON.parse(result.answer) === 'object' ?
            JSON.parse(result.answer)
              .map((answer, index) => answer ? index : 'empty')
              .filter(answer => typeof answer !== 'string')
              .toString()
            : isJsonString(result.answer) ? JSON.parse(result.answer) : result.answer,
        }
      })
    });
    const excelFormatResults = results.map(result => {
      return result.reduce((acc, answer) => {
        const [[key, value], [key2, value2]] = Object.entries(answer);
        acc[key] = value;
        acc[key2] = value2;
        return acc;
      }, {});
    });
    res.status(200).send(excelFormatResults);
  } catch (err) {
    res.status(500).send(err.stack);
  }
}

async function changeSurveyState(req, res) {
  const currentSurvey = await Surveys.findByPk(req.body.surveyId);
  const updatedSurvey = { ...currentSurvey, state: !currentSurvey.state };
  try {
    const updated = await Surveys.update(updatedSurvey, {
      where: {
        survey_id: req.body.surveyId,
      },
      fields: ["state"]
    });
    console.log('-->updated', updated);
    res.status(200).json(updated)
  } catch (err) {
    res.status(500).send(err.stack);
  }
}

module.exports = {
  createSurvey,
  getSurveys,
  adminGetSurveyById,
  clientGetSurveyById,
  createUser,
  saveResults,
  saveSurvey,
  checkPassword,
  getResultsBySurveyId,
  deleteSurvey,
  getResults,
  exportResults,
  changeSurveyState,
};