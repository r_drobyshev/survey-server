const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const surveyControllers = require('../controllers/surveys');
const userControllers = require('../controllers/users');

router.post('/user', surveyControllers.createUser);

router.get('/surveys', auth, surveyControllers.getSurveys);

router.get('/questions/admin/:id', auth, surveyControllers.adminGetSurveyById);

router.get('/results/export', surveyControllers.exportResults);

router.get('/results/:id', auth, surveyControllers.getResultsBySurveyId);

router.get('/questions/survey/:id', surveyControllers.clientGetSurveyById);

router.post('/survey', auth, surveyControllers.createSurvey);

router.put('/survey/:id', auth, surveyControllers.saveSurvey);

router.post('/results/:id', surveyControllers.saveResults);

router.post('/password/:id', surveyControllers.checkPassword);

router.post('/login', userControllers.signIn);

router.post('/delete', surveyControllers.deleteSurvey);

router.post('/state', surveyControllers.changeSurveyState);

router.get('/results', surveyControllers.getResults);

module.exports = router;
