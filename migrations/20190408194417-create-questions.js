'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Questions', {
      question_id: {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false,
      },
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        // autoIncrement: true,
      },
      survey_id: {
        type: Sequelize.STRING,
        onDelete: 'CASCADE',
        references: {
          model: 'Surveys',
          key: 'survey_id',
        },
        allowNull: false,
      },
      block_id: {
        type: Sequelize.STRING
      },
      text: {
        type: Sequelize.STRING
      },
      type: {
        type: Sequelize.STRING
      },
      answer_variants: {
        type: Sequelize.TEXT
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: new Date(),
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: new Date(),
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Questions');
  }
};