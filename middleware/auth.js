const jwt = require('jsonwebtoken');

module.exports = async (req, res, next) => {
  try {
    if (!req.headers.authorization) return res.status(401).json('Пользователь не авторизован!');
    const token = req.headers.authorization.replace('Bearer ', '');
    const payload = jwt.decode(token, '2clX1hP8CTHwxjeS');
    const currentTimeStamp = Math.floor(new Date() / 1000);
    if (currentTimeStamp > payload.exp) return res.status(401).json('Token is expired');
    next();
  } catch(err) {
    console.log('-->err', err);
    res.status(500).json(err);
  }
};
